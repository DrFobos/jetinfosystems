import java.util.ArrayList;
import java.util.Random;


public class Singleton {

    private ArrayList<String> randomList;
    private boolean initialized = false;

    private Singleton() {}

    private static class SingletonHolder {
        private static final Singleton HOLDER_INSTANCE = new Singleton();
    }

    public static Singleton getInstance() {
        return SingletonHolder.HOLDER_INSTANCE;
    }

    public synchronized void populate() {
        if (!initialized){
            initialized = true;
            Random random = new Random();
            ArrayList<String> tempList = new ArrayList<String>();
            for (int i = 0; i < 100; i++) {
                tempList.add(random.nextBoolean() ? "Pass" : "Fail");
            }
            randomList = tempList;
        }
    }

    public synchronized String get(){
        Random random = new Random();
        return this.randomList.get(random.nextInt(100));
    }

    public synchronized void change(int index, String value) {
        if (index < 0 || index >= 100){
            throw new IllegalArgumentException("'Index' argument must be between 0 and 99 inclusively");
        }
        this.randomList.set(index, value);
    }

}
