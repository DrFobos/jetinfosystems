import org.testng.Assert;
import org.testng.annotations.*;
import java.util.Random;


public class SingletonTest {
    private Singleton singletonObj = Singleton.getInstance();


    @BeforeSuite
    public void setUp(){
        singletonObj.populate();
    }


    public class getTestClass {
        @Test(enabled = false)
        public void getTest() {
            Assert.assertTrue(singletonObj.get().contains("ass"));
        }

        @Test(enabled = true)
        public void true_getTest() {
            String request = singletonObj.get();
            Assert.assertTrue(request.contains("Pass") || request.contains("Fail"));
        }
    }

    public class changeTestClass {
        @Test(enabled = true,expectedExceptions = IllegalArgumentException.class)
        @Parameters({"text"})
        public void negative_pointer_changeTest(@Optional("test") String text) throws IllegalArgumentException{
            singletonObj.change(-1, text);
        }

        @Test(enabled = true,expectedExceptions = IllegalArgumentException.class)
        @Parameters({"text"})
        public void big_pointer_changeTest(@Optional("test") String text) throws IllegalArgumentException{
            singletonObj.change(100, text);
            System.out.println("text value: " + text);
        }

        @Test(enabled = true)
        @Parameters({"text"})
        public void normal_pointer_changeTest(@Optional("test") String text) {
            singletonObj.change(99, text);
            singletonObj.change(0, text);
            singletonObj.change(50, text);
        }

        @Test(enabled = true)
        public void void_string_changeTest() {
            Random random = new Random();
            singletonObj.change(random.nextInt(100), "");
        }

        @Test(enabled = false)
        @Parameters({"text"})
        public void brute_changeTest(@Optional("test") String text) {
            for (int i = 0; i < 100; i++) {
                singletonObj.change(i, text);
            }
            Assert.assertTrue(singletonObj.get().equals(text));
        }
    }

//    public class changeTestClass {
//        @Test
//        @Parameters({"text"})
//        public void changeTest(@Optional("test") String text) {
//            Random random = new Random();
//            int before = singletonObj.hashCode();
//            singletonObj.change(random.nextInt(100), text);
//            int after = singletonObj.hashCode();
//            System.out.println("before: " + before);
//            System.out.println("after : " + after);
//            Assert.assertTrue(before != after);
//        }
//    }

}
